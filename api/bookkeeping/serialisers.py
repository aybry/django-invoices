import logging
from pprint import pformat

from rest_framework import serializers, fields

from bookkeeping.models import Entry, DocumentTemplate, HtmlTemplate, Document
from address_book.serialisers import EntitySerializer, BankAccountSerializer

logger = logging.getLogger(__name__)


class EntrySerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    date = fields.DateField(input_formats=["%Y-%m-%d"])
    document_no = serializers.ReadOnlyField(source="document.identifier")

    class Meta:
        model = Entry
        exclude = ["user"]
        read_only_fields = ["client_data"]

    def to_representation(self, instance):
        representation = super(EntrySerializer, self).to_representation(instance)
        if instance.client is not None:
            representation["client"] = EntitySerializer(instance.client).data
        else:
            representation["client"] = {"id": None}

        return representation

    def create(self, validated_data):
        logger.info(pformat(validated_data))
        entry = Entry.objects.create(
            user=self.context["request"].user, **validated_data
        )
        return entry

    def update(self, instance: Entry, validated_data):
        logger.info(pformat(validated_data))
        Entry.objects.filter(pk=instance.pk).update(**validated_data)
        instance = Entry.objects.get(pk=instance.pk)
        return instance


class DocumentTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentTemplate
        fields = "__all__"


class HtmlTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = HtmlTemplate
        fields = "__all__"


class DocumentSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    entries = EntrySerializer(many=True)
    recipient = EntitySerializer()
    sender = EntitySerializer()
    bank_account = BankAccountSerializer()

    class Meta:
        model = Document
        fields = "__all__"

    def to_representation(self, instance):
        representation = super(DocumentSerializer, self).to_representation(instance)
        representation["date_str"] = instance.date.strftime("%d %B %Y")
        return representation
