import re

from datetime import datetime

from django.core.exceptions import ValidationError


def idenifier_template_validator(value: str):
    dt_now = datetime.now()
    try:
        value.format(
            year=dt_now.year,
            month=str(dt_now.month).zfill(2),
            day=str(dt_now.day).zfill(2),
            hour=str(dt_now.hour).zfill(2),
            minute=str(dt_now.minute).zfill(2),
            second=str(dt_now.second).zfill(2),
            # counter
        )
        assert value.count("{counter}") <= 1
    except ValueError:
        raise ValidationError(
            "Available values are: '{year}', '{month}', "
            "'{day}', '{hour}', '{minute}', '{second}', '{counter}'"
        )
    except AssertionError:
        raise ValidationError("'{counter}' can only be used once.")


def tax_year_validator(value):
    if not isinstance(value, int) and value >= 2000 and value < 2100:
        raise ValidationError("Tax year invalid")


def document_identifier_validator(value):
    if len(re.findall("[a-zA-Z0-9-_]*", value)) == 0:
        raise ValidationError(
            "Identifier must be lower-/upper-case alphanumeric "
            "separated by hyphen or underscore (- or _). "
            f"Invalid identifier: {value}"
        )
