class BookkeepingBaseException(Exception):
    pass


class NoTemplateFoundException(BookkeepingBaseException):
    pass


class AmbiguousEntriesException(BookkeepingBaseException):
    pass


class DocumentIsFinalException(BookkeepingBaseException):
    pass


class PictureThisResponseException(BookkeepingBaseException):
    pass


class DifferingUnitPriceException(BookkeepingBaseException):
    pass
