import logging

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from bookkeeping.models import Entry, DocumentTemplate
from address_book.models import Entity


logger = logging.getLogger(__name__)
User = get_user_model()


class Command(BaseCommand):
    help = "Creates Quote via command line"

    def add_arguments(self, parser):
        parser.add_argument("-n", "--count", nargs="?")
        parser.add_argument("-p", "--unit-price", nargs="?", default=480)
        parser.add_argument("-e", "--entity-slug", nargs="?")
        parser.add_argument("-d", "--description", nargs="?")
        parser.add_argument("-t", "--template-name", nargs="?", default="Quote DE")

    def handle(self, *args, **options):
        count = int(options["count"])
        entity_slug = options["entity_slug"]
        description = options["description"]
        unit_price = float(options["unit_price"])
        template_name = options["template_name"]

        entity = Entity.objects.get(slug=entity_slug)
        user_aybry = User.objects.get(pk=1)

        entry = Entry.objects.create(
            user=user_aybry,
            description=description,
            unit_price=unit_price,
            unit_number=count,
            client=entity,
        )
        entry_queryset = Entry.objects.filter(pk=entry.pk)

        print(
            f"Created entry: {entry}\n\tClient: {entry.client}\n\tDescription: {entry.description}\n\tHours: {entry.unit_number} @ {entry.unit_price} € ({float(entry.subtotal)} €)\n\t"
        )

        quote_template_de = DocumentTemplate.objects.get(name=template_name)
        quote = quote_template_de.initialise()
        quote.entries.set(entry_queryset)
        quote.recipient = entity
        quote.save()
        quote.render()

        print(
            f"Created quote: {quote.identifier}\n\tClient: {quote.recipient}\n\tItem price: {entry.unit_number} @ {entry.unit_price} € ({float(entry.subtotal)} €)\n\t"
        )
