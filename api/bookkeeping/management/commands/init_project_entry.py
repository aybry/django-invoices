import logging

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from bookkeeping.models import ProjectEntry
from address_book.models import Entity


logger = logging.getLogger(__name__)
User = get_user_model()


class Command(BaseCommand):
    help = "Initialises ProjectEntry via command line"

    def add_arguments(self, parser):
        parser.add_argument("-e", "--entity-slug", nargs="?")
        parser.add_argument("-p", "--unit-price", nargs="?", default=60)

    def handle(self, *args, **options):
        entity_slug = options["entity_slug"]
        unit_price = options["unit_price"]

        entity = Entity.objects.get(slug=entity_slug)
        user_aybry = User.objects.get(pk=1)

        entry = ProjectEntry.objects.create(
            user=user_aybry,
            unit_price=unit_price,
            client=entity,
        )
        entry = ProjectEntry.objects.get(pk=entry.pk)

        print(
            f"Created ProjectEntry: {entry}\n\tClient: {entry.client}\n\tPrice: {entry.unit_price} €\n\t"
        )
