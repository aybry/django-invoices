import logging

from django.contrib.auth import get_user_model
from rest_framework import viewsets, permissions
from rest_framework.response import Response

from project.serialisers import UserSerializer
from bookkeeping.models import Entry, HtmlTemplate, DocumentTemplate, Document
from bookkeeping.serialisers import (
    EntrySerializer,
    DocumentTemplateSerializer,
    HtmlTemplateSerializer,
    DocumentSerializer,
)


logger = logging.getLogger(__name__)


class ViewSetWithRequestContext(viewsets.ModelViewSet):
    def get_serializer_context(self):
        return {"request": self.request}


class RenderDocumentViewSet(viewsets.ViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = [permissions.IsAuthenticated]

    def retrieve(self, _, pk):
        document = Document.objects.get(id=pk)
        output_path = document.render()
        return Response(str(output_path))


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class EntryViewSet(ViewSetWithRequestContext):
    serializer_class = EntrySerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return Entry.objects.filter(user=user).order_by("-date")


class DocumentTemplateViewSet(viewsets.ModelViewSet):
    queryset = DocumentTemplate.objects.all()
    serializer_class = DocumentTemplateSerializer
    permission_classes = [permissions.IsAuthenticated]


class HtmlTemplateViewSet(viewsets.ModelViewSet):
    queryset = HtmlTemplate.objects.all()
    serializer_class = HtmlTemplateSerializer
    permission_classes = [permissions.IsAuthenticated]


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.filter(document_type="in")
    serializer_class = DocumentSerializer
    permission_classes = [permissions.IsAuthenticated]


class QuoteViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.filter(document_type="qt")
    serializer_class = DocumentSerializer
    permission_classes = [permissions.IsAuthenticated]
