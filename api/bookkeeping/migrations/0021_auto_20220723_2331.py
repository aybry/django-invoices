# Generated by Django 3.2.14 on 2022-07-23 23:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeping', '0020_auto_20220723_2310'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='bottom_text',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='document_number_label',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='farewell',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='greeting',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='header',
            field=models.CharField(default='', max_length=32),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='document',
            name='top_text',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='documenttemplate',
            name='bottom_text',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='documenttemplate',
            name='document_number_label',
            field=models.CharField(default='', max_length=64),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='documenttemplate',
            name='farewell',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='documenttemplate',
            name='greeting',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='documenttemplate',
            name='header',
            field=models.CharField(default='', max_length=32),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='documenttemplate',
            name='top_text',
            field=models.TextField(default='', max_length=4096),
            preserve_default=False,
        ),
    ]
