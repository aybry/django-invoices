# Generated by Django 3.2.14 on 2022-07-15 21:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeping', '0005_add_invoice_language'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='invoice',
            name='language',
        ),
        migrations.AddField(
            model_name='invoice',
            name='locale',
            field=models.CharField(choices=[('en_GB', 'English'), ('de_DE', 'German')], default='en_GB', max_length=5),
        ),
    ]
