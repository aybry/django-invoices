# Generated by Django 3.2.14 on 2022-07-23 22:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeping', '0014_document_number_label'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='document_type',
            field=models.CharField(choices=[('in', 'Invoice'), ('qt', 'Quote')], default='in', max_length=2),
            preserve_default=False,
        ),
    ]
