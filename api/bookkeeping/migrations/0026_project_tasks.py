# Generated by Django 3.2.16 on 2022-12-07 14:29

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeping', '0025_documenttemplate_logo_uuid'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entry',
            name='project',
        ),
        migrations.AddField(
            model_name='project',
            name='name',
            field=models.CharField(default='', max_length=64),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('description', models.TextField(max_length=1024)),
                ('quantity', models.DecimalField(decimal_places=4, max_digits=12)),
                ('entry', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tasks', to='bookkeeping.entry')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='tasks', to='bookkeeping.project')),
            ],
        ),
    ]
