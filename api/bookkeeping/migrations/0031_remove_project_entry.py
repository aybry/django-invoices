# Generated by Django 3.2.16 on 2023-01-13 23:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("bookkeeping", "0030_entry_project"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="task",
            name="entry",
        ),
        migrations.RemoveField(
            model_name="task",
            name="project",
        ),
        migrations.DeleteModel(
            name="ProjectEntry",
        ),
        migrations.DeleteModel(
            name="Task",
        ),
    ]
