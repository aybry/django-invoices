import datetime
import requests
import uuid
import yaml
from decimal import Decimal
from pathlib import Path
from typing import Union

from babel.dates import format_date
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from jinja2 import Environment, FileSystemLoader, select_autoescape
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator

from address_book.models import Entity, BankAccount
from bookkeeping.utils import entry_date_to_str
from bookkeeping.validators import (
    idenifier_template_validator,
    tax_year_validator,
)
from bookkeeping.exceptions import (
    AmbiguousEntriesException,
    NoTemplateFoundException,
    DocumentIsFinalException,
    PictureThisResponseException,
    DifferingUnitPriceException,
)
from project.logger import logger


User = get_user_model()


class Project(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    identifier = models.CharField(max_length=64)
    name = models.CharField(max_length=64, default="")


class LocaleChoices(models.TextChoices):
    EN = ("en_GB", "English")
    DE = ("de_DE", "German")


class DocumentTypeChoices(models.TextChoices):
    INVOICE = ("in", "Invoice")
    QUOTE = ("qt", "Quote")


class Entry(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date = models.DateField(auto_now_add=True)
    description = models.TextField(max_length=1024)
    unit_price = models.DecimalField(decimal_places=10, max_digits=16)
    unit_number = models.DecimalField(decimal_places=4, max_digits=12)
    client = models.ForeignKey(Entity, on_delete=models.SET_NULL, null=True)
    document = models.ForeignKey(
        "Document", on_delete=models.SET_NULL, null=True, related_name="entries"
    )
    is_paid = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="entries",
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("-date",)

    @property
    def subtotal(self) -> Decimal:
        return self.unit_price * self.unit_number

    @classmethod
    def list_unpaid(cls, client_in: Union[Entity, str] = None) -> models.QuerySet:
        items = cls.objects.filter(is_paid=False).filter(
            models.Q(document__isnull=True)
            | ~models.Q(document__document_type=DocumentTypeChoices.QUOTE)
        )
        if client_in:
            if isinstance(client_in, str):
                try:
                    client = Entity.objects.get(pk=client_in)
                except ValidationError:
                    client = Entity.objects.get(slug=client_in)
            else:
                client = client_in
            items = items.filter(client=client)

        return items

    @classmethod
    def sum_unpaid(cls, client: Union[Entity, str] = None) -> Decimal:
        unpaid_items = cls.list_unpaid(client)
        return sum(entry.subtotal for entry in unpaid_items)

    @property
    def date_as_str(self) -> str:
        return entry_date_to_str(self.date, self.document.locale)

    # @property
    # def project_hours(self):
    #     project_hours = {}
    #     for task in self.tasks.all():
    #         try:
    #             project_hours[task.project] += task.quantity
    #         except KeyError:
    #             project_hours[task.project] = task.quantity

    #     return project_hours


class IdentifierGenerator(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    template = models.CharField(
        max_length=128, validators=[idenifier_template_validator]
    )
    document_type = models.CharField(max_length=2, choices=DocumentTypeChoices.choices)
    counter_digits = models.IntegerField(
        default=1, validators=[MaxValueValidator(5), MinValueValidator(1)]
    )

    def generate(self):
        dt_now = datetime.datetime.now()
        try:
            prefix_template, suffix_template = self.template.split("{counter}")
            prefix, suffix = self._format(prefix_template, dt_now), self._format(
                suffix_template, dt_now
            )
            next_count = (
                Document.objects.filter(
                    document_type=self.document_type,
                    identifier__startswith=prefix,
                    identifier__endswith=suffix,
                ).count()
                + 1
            )
        except ValueError:  # counter not used
            next_count = 1

        return self._format(
            dt=dt_now, counter=str(next_count).zfill(self.counter_digits)
        )

    def _format(
        self,
        template_str: str = None,
        dt: datetime.datetime = None,
        counter: str = None,
    ):
        if template_str is None:
            template_str = self.template
        if dt is None:
            dt = datetime.datetime.now()

        return template_str.format(
            year=dt.year,
            month=str(dt.month).zfill(2),
            day=str(dt.day).zfill(2),
            hour=str(dt.hour).zfill(2),
            minute=str(dt.minute).zfill(2),
            second=str(dt.second).zfill(2),
            counter=counter,
        )


class HtmlTemplate(models.Model):
    name = models.CharField(max_length=64)


class DocumentTemplate(models.Model):
    name = models.CharField(max_length=64)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    document_type = models.CharField(max_length=2, choices=DocumentTypeChoices.choices)

    identifier_generator = models.ForeignKey(
        IdentifierGenerator, on_delete=models.SET_NULL, null=True
    )
    sender = models.ForeignKey(
        Entity, on_delete=models.SET_NULL, null=True, related_name="template_out"
    )
    recipient = models.ForeignKey(
        Entity, on_delete=models.SET_NULL, null=True, related_name="template_in"
    )
    bank_account = models.ForeignKey(BankAccount, on_delete=models.SET_NULL, null=True)

    value_added_tax_percent = models.DecimalField(
        decimal_places=2, max_digits=8, default=19
    )
    locale = models.CharField(
        max_length=5, choices=LocaleChoices.choices, default=LocaleChoices.EN
    )

    header = models.CharField(max_length=32)
    document_number_label = models.CharField(max_length=64)
    greeting = models.TextField(max_length=4096)
    top_text = models.TextField(max_length=4096)
    bottom_text = models.TextField(max_length=4096)
    farewell = models.TextField(max_length=4096)

    logo_uuid = models.UUIDField(default=None, null=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return f"{self.name} (ID: {self.pk})"

    def initialise(self) -> "Document":
        doc = Document.objects.create(
            user=self.user,
            document_type=self.document_type,
            sender=self.sender,
            recipient=self.recipient,
            bank_account=self.bank_account,
            value_added_tax_percent=self.value_added_tax_percent,
            locale=self.locale,
            header=self.header,
            document_number_label=self.document_number_label,
            greeting=self.greeting,
            top_text=self.top_text,
            bottom_text=self.bottom_text,
            farewell=self.farewell,
            logo_uuid=self.logo_uuid,
        )

        if self.identifier_generator:
            doc.identifier = self.identifier_generator.generate()
        else:
            doc.identifier = doc.id.split("-")[0]
        doc.save()

        return doc


class Document(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    document_type = models.CharField(max_length=2, choices=DocumentTypeChoices.choices)

    date = models.DateField(auto_now_add=True)
    timeframe_start = models.DateField(null=True)
    timeframe_end = models.DateField(null=True)
    due_by_date = models.DateField(null=True)
    value_added_tax_percent = models.DecimalField(
        decimal_places=2, max_digits=8, default=19
    )
    locale = models.CharField(
        max_length=5, choices=LocaleChoices.choices, default=LocaleChoices.EN
    )

    identifier = models.CharField(max_length=64, null=True)
    sender = models.ForeignKey(
        Entity, on_delete=models.SET_NULL, null=True, related_name="document_out"
    )
    recipient = models.ForeignKey(
        Entity, on_delete=models.SET_NULL, null=True, related_name="document_in"
    )
    bank_account = models.ForeignKey(BankAccount, on_delete=models.SET_NULL, null=True)

    header = models.CharField(max_length=32)
    document_number_label = models.CharField(max_length=64)
    greeting = models.TextField(max_length=4096)
    top_text = models.TextField(max_length=4096)
    bottom_text = models.TextField(max_length=4096)
    farewell = models.TextField(max_length=4096)

    is_final = models.BooleanField(default=False)
    is_sent = models.BooleanField(default=False)
    is_paid = models.BooleanField(default=False)
    tax_year = models.IntegerField(validators=[tax_year_validator], null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    logo_uuid = models.UUIDField(default=None, null=True)

    class Meta:
        ordering = ("identifier",)

    def __repr__(self):
        return f"<Document: {self.identifier}>"

    def _format_date(self, date_obj: datetime.date) -> str:
        return date_obj.strftime("%d %B %Y")

    @property
    def template_name(self):
        if self.document_type == DocumentTypeChoices.INVOICE:
            if self.is_project_based:
                return "invoice_projects.json.jinja"
            else:
                return "invoice.json.jinja"
        elif self.document_type == DocumentTypeChoices.QUOTE:
            return "quote.json.jinja"
        else:
            raise NoTemplateFoundException("No template for this document type")

    @property
    def total_net(self) -> Decimal:
        return Decimal(sum([entry.subtotal for entry in self.entries.all()]))

    @property
    def date_as_str(self) -> str:
        return format_date(self.date, format="full", locale=self.locale)

    @property
    def due_by_date_as_str(self) -> str:
        return self._format_date(self.due_by_date)

    @property
    def tax(self) -> Decimal:
        return Decimal(self.value_added_tax_percent / 100 * self.total_net)

    @property
    def total_gross(self) -> Decimal:
        return Decimal(self.total_net + self.tax)

    @property
    def timeframe(self) -> str:
        return f"{self.timeframe_start.strftime('%d.%m.%Y')} - {self.timeframe_end.strftime('%d.%m.%Y')}"

    @property
    def is_project_based(self):
        if (
            self.entries.filter(project__isnull=True).exists()
            and self.entries.filter(project__isnull=False).exists()
        ):
            raise AmbiguousEntriesException(
                f"{self} contains tasks with and without projects!"
            )

        return (
            self.entries.filter(project__isnull=False).exists()
            and not self.entries.filter(project__isnull=True).exists()
        )

    @property
    def entries_grouped_by_date(self):
        entries_by_date = {}

        for entry in self.entries.all().order_by("date"):
            try:
                entries_by_date[entry.date_as_str]["entries"].append(entry)
                entries_by_date[entry.date_as_str]["subtotal"] += entry.subtotal
                entries_by_date[entry.date_as_str][
                    "quantity_total"
                ] += entry.unit_number
                if (
                    not entries_by_date[entry.date_as_str]["unit_price"]
                    == entry.unit_price
                ):
                    raise DifferingUnitPriceException(
                        f"{entry.unit_price} is not equal to {entries_by_date[entry.date_as_str]['unit_price']}"
                    )

            except KeyError:
                entries_by_date[entry.date_as_str] = {
                    "entries": [entry],
                    "quantity_total": entry.unit_number,
                    "subtotal": entry.subtotal,
                    "unit_price": entry.unit_price,
                }
        return entries_by_date

    def mark_paid(self):
        self.entries.update(is_paid=True)
        self.is_paid = True
        self.save()

    def render(self):
        if self.is_final:
            raise DocumentIsFinalException(
                "Can't re-render a document that is marked final."
            )

        env = Environment(
            loader=FileSystemLoader(settings.BASE_DIR / "bookkeeping" / "templates"),
            autoescape=select_autoescape(),
        )

        picture_this_directory: Path = Path("picture_this")

        doc_output_path = settings.BOOKKEEPING_DATA_DIR / "test.pdf"
        logger.debug(f"Rendering to output path {doc_output_path}")

        template_path = picture_this_directory / self.template_name

        template = env.get_template(str(template_path))
        rendered = template.render(doc=self)
        logger.debug("JSON rendered successfully")
        data_dict = yaml.load(rendered, Loader=yaml.FullLoader)

        try:
            res = requests.post(
                settings.PICTURE_THIS_RENDER_URL,
                json=data_dict,
                headers={
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },
            )
            if res.status_code == 200:
                logger.debug("PDF rendered successfully")
                with open(
                    settings.BOOKKEEPING_DATA_DIR / "pdf" / "test.pdf", "wb+"
                ) as f:
                    f.write(res.content)
            else:
                raise PictureThisResponseException(res.text)

        except Exception as exc:
            logger.error(data_dict)
            logger.exception("Could not render")
            raise exc

        return doc_output_path
