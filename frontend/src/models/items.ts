export default Item = {
  date: string,
  description: string,
  unit_price: integer,
  unit_number: number,
  is_paid: boolean,
};
