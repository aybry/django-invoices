import axiosInstance from "@/services/api";

const namespaced = true;

const state = {
  invoices: [],
  current: null,
  status: "",
  last_update: "",
};

const getters = {
  data: (state) => state.invoices,
  current: (state) => state.current,
};

const mutations = {
  request(state) {
    state.status = "loading";
  },

  fetch_success(state, payload) {
    state.status = "success";
    state.invoices = payload;
    state.last_update = new Date();
  },

  create_success(state, payload) {
    state.status = "success";
    state.invoices.unshift(payload);
  },

  update_success(state, payload) {
    state.status = "success";
    const invoiceIndex = state.invoices.findIndex(
      (invoice) => invoice.id == payload.id
    );
    state.invoices[invoiceIndex] = payload;
  },

  delete_success(state, payload) {
    state.status = "success";
    const invoiceIndex = state.invoices.findIndex(
      (invoice) => invoice.id == payload
    );
    state.invoices.splice(invoiceIndex, 1);
  },

  error(state, payload) {
    state.status = "error";
  },

  new_current(state, payload) {
    state.current = payload;
  },
};

const actions = {
  get({ commit }, invoiceId) {
    commit("request");
    axiosInstance.get(`/invoices/${invoiceId}`).then((res) => {
      commit("new_current", res.data);
      console.log(res.data);
    });
  },

  fetch({ commit }) {
    commit("request");
    axiosInstance.get("/invoices/").then((res) => {
      commit("fetch_success", res.data);
      console.log(res.data);
    });
  },

  create: ({ commit }, invoice) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .post("/invoices/", invoice)
        .then((resp) => {
          const invoice = resp.data;
          commit("create_success", invoice);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  update: ({ commit }, invoice) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .put(`/invoices/${invoice.id}/`, invoice)
        .then((resp) => {
          const invoice = resp.data;
          commit("update_success", invoice);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  delete: ({ commit }, invoice) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .delete(`/invoices/${invoice.id}/`, invoice)
        .then((resp) => {
          commit("delete_success", resp.config.id);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  set_current({ commit }, invoiceId) {
    commit("new_current", invoiceId);
  },
};

const invoiceStoreModule = {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};

export default invoiceStoreModule;
