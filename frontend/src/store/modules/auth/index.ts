import axiosInstance from "@/services/api";

const namespaced = true;

const state = {
  status: "",
  token: localStorage.getItem("token") || "",
  refresh: localStorage.getItem("refresh") || "",
  user: {},
};

const getters = {
  isAuthenticated: (state) => !!state.token,
  authStatus: (state) => state.status,
};

const mutations = {
  auth_request(state) {
    state.status = "loading";
  },

  auth_success(state, payload) {
    state.status = "success";
    state.token = payload.token;
    state.refresh = payload.refresh;
    // state.user = payload.user;
  },

  refresh_success(state, payload) {
    state.status = "success";
    state.token = payload.token;
  },

  auth_error(state) {
    state.status = "error";
  },

  logout(state) {
    state.status = "";
    state.token = "";
    state.refresh = "";
  },
};

const actions = {
  login: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      commit("auth_request");
      axiosInstance
        .post("/api/token/", user)
        .then((resp) => {
          const token = resp.data.access;
          const refresh = resp.data.refresh;
          // const user = resp.data.user;

          localStorage.setItem("token", token);
          localStorage.setItem("refresh", refresh);
          axiosInstance.defaults.headers.common["Authorization"] = `Bearer ${token}`;
          commit("auth_success", { token: token, refresh: refresh });
          resolve(resp);
        })
        .catch((err) => {
          commit("auth_error");
          localStorage.removeItem("token");
          reject(err);
        });
    });
  },

  refresh: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit("auth_request");
      axiosInstance
      .post("api/token/refresh/", {
        refresh: state.refresh
      }).then((resp) => {
        const token = resp.data.access;
        localStorage.setItem("token", token);
        axiosInstance.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        commit("refresh_success", { token: token });
        resolve(token);
      })
      .catch((err) => {
        commit("auth_error");
        localStorage.removeItem("token");
        localStorage.removeItem("refresh");
        reject(err);
      })
    })
  },

  register: ({ commit }, user) => {
    return new Promise((resolve, reject) => {
      commit("auth_request");
      axiosInstance
        .post("/register/", user)
        .then((resp) => {
          const token = resp.data.token;
          const user = resp.data.user;

          localStorage.setItem("token", token);
          axiosInstance.defaults.headers.common["Authorization"] = token;
          commit("auth_success", { token: token, user: user });
          resolve(resp);
        })
        .catch((err) => {
          commit("auth_error", err);
          localStorage.removeItem("token");
          reject(err);
        });
    });
  },

  logout: ({ commit }) => {
    return new Promise((resolve, reject) => {
      commit("logout");
      localStorage.removeItem("token");
      localStorage.removeItem("refresh");
      delete axiosInstance.defaults.headers.common["Authorization"];
      console.log("logged out");
      resolve();
    });
  },
};

const authStoreModule = {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};

export default authStoreModule;
