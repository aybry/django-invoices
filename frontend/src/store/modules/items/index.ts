import axiosInstance from "@/services/api";

const namespaced = true;

const state = {
  items: [],
  current: null,
  status: "",
  last_update: "",
};

const getters = {
  data: (state) => state.items,
  current: (state) => state.current,
};

const mutations = {
  request(state) {
    state.status = "loading";
  },

  fetch_success(state, payload) {
    state.status = "success";
    state.items = payload;
    state.last_update = new Date();
  },

  create_success(state, payload) {
    state.status = "success";
    state.items.unshift(payload);
  },

  update_success(state, payload) {
    state.status = "success";
    console.log("updated");
    const itemIndex = state.items.findIndex((item) => item.id == payload.id);
    console.log(itemIndex);
    state.items[itemIndex] = payload;
    console.log(state.items[itemIndex]);
    console.log(payload);
  },

  delete_success(state, payload) {
    state.status = "success";
    const itemIndex = state.items.findIndex((item) => item.id == payload);
    state.items.splice(itemIndex, 1);
  },

  error(state, payload) {
    state.status = "error";
  },

  new_current(state, payload) {
    state.current = payload;
  },

  sort(state) {
    state.items = state.items.sort((firstEl, secondEl) => {
      const firstDate = firstEl.date;
      const secondDate = secondEl.date;
      if (firstDate < secondDate) {
        return 1;
      } else if (firstDate > secondDate) {
        return -1;
      } else if (firstDate == secondDate) {
        return 0;
      }
    });
  },
};

const actions = {
  fetch({ commit }) {
    commit("request");
    axiosInstance.get("/entries/").then((res) => {
      console.log("items", res.data);
      commit("fetch_success", res.data);
      // commit("sort");
    });
  },

  create: ({ commit }, item) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .post("/entries/", item)
        .then((resp) => {
          const item = resp.data;
          commit("create_success", item);
          // commit("sort");
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  update: ({ commit }, item) => {
    commit("request");
    console.log("item", item);
    return new Promise((resolve, reject) => {
      axiosInstance
        .put(`/entries/${item.id}/`, item)
        .then((resp) => {
          const item = resp.data;
          commit("update_success", item);
          // commit("sort");
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  delete: ({ commit }, item) => {
    commit("request");
    return new Promise((resolve, reject) => {
      axiosInstance
        .delete(`/entries/${item.id}/`, item)
        .then((resp) => {
          commit("delete_success", resp.config.id);
          resolve(resp);
        })
        .catch((err) => {
          commit("error");
          reject(err);
        });
    });
  },

  set_current({ commit }, item) {
    commit("new_current", item);
  },
};

const itemStoreModule = {
  namespaced,
  state,
  mutations,
  actions,
  getters,
};

export default itemStoreModule;
