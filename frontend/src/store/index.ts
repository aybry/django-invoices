import { createStore } from "vuex";
import auth from "./modules/auth";
import items from "./modules/items";
import invoices from "./modules/invoices";
import quotes from "./modules/quotes";
import modals from "./modules/modals";
import addressBook from "./modules/addressBook";

export default createStore({
  modules: {
    items,
    invoices,
    quotes,
    modals,
    auth,
    addressBook,
  },
});
