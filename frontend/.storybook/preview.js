import { app } from '@storybook/vue3'
import naive from 'naive-ui'

app.use(naive)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
